c = $("#c")[0];
d = c.getContext('2d');

var S = 9;
var T = 2 * S - 1;

var look_dir = null;
var show_dists = false;
var show_soln = false;
var SPACE = 30;
var RAD = 2;
var hex_size = 17;
var global_off = {x:SPACE * S / 2, y:SPACE};
var level = 1;

w = c.width = T * SPACE;
h = c.height = (T - 1) * SPACE;

var g_dists = {};
var g_prevs = {};

var key_table = {
  75: 0,
  73: 1,
  85: 2,
  72: 3,
  78: 4,
  77: 5,
};

key_table[36] = key_table[85];
key_table[33] = key_table[73];
key_table[37] = key_table[72];
key_table[39] = key_table[75];
key_table[35] = key_table[78];
key_table[34] = key_table[77];

var dir_table = [
  {x:1, y:0},
  {x:0, y:-1},
  {x:-1, y:-1},
  {x:-1, y:0},
  {x:0, y:1},
  {x:1, y:1},
];

function init() {
  while (1) {
    look_dir = null;
    undo_history = [];
    mat = gen_mat();
    player = gen_player();
    goal = gen_goal();
    if (difficulty > level) break;
  }
  render();
  $("#moves").text(0);
}

function gen_mat() {
  var mat = [];
  for (var i = 0; i < T; i++) {
    mat[i] = [];
    for (var j = 0; j < T; j++) {
      if (i - j >= S || j - i >= S) mat[i][j] = 0;
      else mat[i][j] = Math.random() > 0.15 ? 1 : 2;
    }
  }
  return mat;
}

function hex_transform(p) {
  return  {x: p.x * SPACE - p.y * SPACE / 2,
	   y: p.y * SPACE * Math.sqrt(3) / 2};
}

function circle(d, p, r) {
  var tp = hex_transform(p);
  d.arc(tp.x, tp.y, r, 0, Math.PI * 2);
}

function star(d, p, R) {
  var tp = hex_transform(p);
  d.save();
  d.translate(tp.x, tp.y);
  r = R / 2.618;
  d.moveTo(0,-R);
  for (var i = 0; i < 5; i++) {
    d.rotate(Math.PI / 5); d.lineTo(0,-r);
    d.rotate(Math.PI / 5); d.lineTo(0,-R);
  }
  d.restore();
}

function render() {
  d.clearRect(0,0,w,h);
  d.save();
  d.translate(global_off.x, global_off.y);
  for (var i = 0; i < T; i++) {
    for (var j = 0; j < T; j++) {
      d.fillStyle = ["white", "#cce", "#99c"][mat[i][j]];
      d.beginPath();
      if (mat[i][j] > 1) {
        d.save();
        var off = hex_transform({x:i, y:j});
        d.translate(off.x, off.y);
        d.moveTo(0,hex_size);
        d.rotate(Math.PI / 3); d.lineTo(0,hex_size);
        d.rotate(Math.PI / 3); d.lineTo(0,hex_size);
        d.rotate(Math.PI / 3); d.lineTo(0,hex_size);
        d.rotate(Math.PI / 3); d.lineTo(0,hex_size);
        d.rotate(Math.PI / 3); d.lineTo(0,hex_size);
//        circle(d, {x:0, y:0}, rad);
        d.restore();

        //

        // d.moveTo(-10,0);
        // d.lineTo(0,10);
        // d.closePath();
        // d.fill();
        // d.restore();
      } else {
        circle(d, {x:i, y:j}, RAD);
      }
      d.fill();
    }
  }

  if (show_soln) {
    var cur = goal;
    while (1) {
      var prev = g_prevs.get(cur.x, cur.y);
      if (prev == null) break;

      d.beginPath();
      var cur_t = hex_transform(cur);
      var prev_t = hex_transform(prev);
      d.moveTo(cur_t.x, cur_t.y);
      d.lineTo(prev_t.x, prev_t.y);
      d.stroke();
      cur = prev;
    }
  }

  if (show_dists) {
    for (var i = 0; i < T; i++) {
      for (var j = 0; j < T; j++) {
        var dist = g_dists.get(i, j);
        if (dist > 0) {
          var gray = "#bbb";
          d.beginPath();
          d.strokeStyle = gray;
          d.fillStyle = "white";
          circle(d, {x:i, y:j}, 10);
          d.fill();
          d.stroke();
          var p = hex_transform({x:i, y:j});
          var height = 14;
          d.font = height + "px sans-serif";
          var width = d.measureText(dist).width;
          d.fillStyle = gray;
          d.fillText(dist, p.x - width/2, p.y + height * 0.35);
        }
      }
    }
  }

  d.beginPath();
  star(d, goal, RAD + 10);
  d.strokeStyle = "#f50";
  d.fillStyle = "#fe0";
  if (show_dists) {
    d.save();
    d.globalAlpha = 0.5;
    d.fill();
    d.restore();
  }
  else {
    d.fill();
  }
  d.stroke();

  d.beginPath();
  circle(d, player, RAD + 10);
  d.fillStyle = "#336";
  d.fill();

  if (look_dir != null) {
    d.save();
    d.beginPath();
    var tp = hex_transform(player);
    d.translate(tp.x, tp.y);
    d.rotate(-look_dir * Math.PI / 3);
    d.arc(9 - RAD, 0, RAD, 0, Math.PI * 2);
    d.fillStyle = "white";
    d.fill();
    d.restore();
  }

 d.restore();
}

var ready = true;

$(document).on('keydown', function(e) {
  console.log(e.keyCode);
  if (ready) {
    if (e.keyCode == 90) { // z
      undo();
    }
    else if (e.keyCode == 65) { // a
      show_dists = !show_dists;
      render();
    }
    else if (e.keyCode == 66) { // b
      show_soln = !show_soln;
      render();
    }
    else if (e.keyCode in key_table) {
      move(key_table[e.keyCode]);
    }
  }
});

function relpos(e) {
  var off = $("#c").offset();
  return {x:e.pageX - off.left, y:e.pageY - off.top};
}

function dir_ix_of_pos(p) {
  var q = hex_transform(player);
  var theta = Math.atan2(global_off.y + q.y - p.y, global_off.x + q.x - p.x);
  var ix = Math.floor(6 * (0.5 - theta / (2 * Math.PI)) + 0.5) % 6;
  return ix;
}

function mousedown (e) {
  var ix = dir_ix_of_pos(relpos(e));
  console.log(ix);
  move(ix);
  e.preventDefault();
}

$(document).on('mousedown', mousedown);

function mousemove (e) {
  last_pos = relpos(e);
  look_dir = dir_ix_of_pos(last_pos);
  e.preventDefault();
  render();
}

$(document).on('mousemove', mousemove);

function clear(p) {
  return (!(p.x < 0 || p.y < 0 || p.x >= T || p.y >= T || mat[p.x][p.y] != 1));
}

function get_result(pos, dp) {
  var count = 0;
  while (1) {
    var prov = {x: pos.x + dp.x, y: pos.y + dp.y};
    if (!clear(prov)) break;
    count++;
    pos = prov;
  }
  return {pos:pos, count:count};
}

function move(dpix) {
  look_dir = dpix;
  var dp = dir_table[dpix];
  anim_src = {x:player.x, y:player.y};
  var result = get_result(player, dp);
  anim_dest = result.pos;
  if (result.count > 0) {
    undo_history.push({x:player.x, y:player.y});
    ready = false;
    anim_t = 0;
    anim_iv = setInterval(animate(1 / result.count), 10);
  }
  $("#moves").text(undo_history.length);
}

function undo() {
  if (undo_history.length > 0) {
    var p = undo_history.pop();
    player.x = p.x;
    player.y = p.y;
    render();
    $("#moves").text(undo_history.length);
  }
}

function animate(inc) {
  return function() {
    anim_t += inc / 3; // 3 frames per cell
    if (anim_t >= 1) {
      player.x = anim_dest.x;
      player.y = anim_dest.y;
      look_dir = dir_ix_of_pos(last_pos);
      ready = true;
      clearInterval(anim_iv);
      if (player.x == goal.x && player.y == goal.y) {
        level++;
        $("#level").text(level);
	init();
      }
    }
    else {
      player.x = anim_t * anim_dest.x + (1-anim_t) * anim_src.x;
      player.y = anim_t * anim_dest.y + (1-anim_t) * anim_src.y;
    }
    render();
  }
}

function gen_player() {
  var player;
  while(1) {
    player = {x: Math.floor(Math.random() * T),
	      y: Math.floor(Math.random() * T)};
    if (clear(player))
      return player;
  }
}

function gen_goal() {
  var seen = {};
  var dists = {};
  var prevs = {};
  var queue = [];
  var worst_dist = 0;
  function see(p, dist, prev) {
    queue.push({x:p.x,y:p.y});
    seen[p.x + "," + p.y] = 1;
    dists[p.x + "," + p.y] = dist;
    prevs[p.x + "," + p.y] = prev;
    worst_dist = Math.max(dist, worst_dist);
  }
  function is_seen(p) {
    return seen[p.x + "," + p.y];
  }
  function get_dist(p) {
    return dists[p.x + "," + p.y];
  }
  see(player, 0, null);
  while (queue.length) {
    var spot = queue.shift();
    dir_table.forEach(function(dir) {
      var result = get_result(spot, dir);
      if (!is_seen(result.pos)) {
       see(result.pos, get_dist(spot) + 1, spot);
      }
    });
  }
  g_dists = dists;
  g_prevs = prevs;
  g_prevs.get = g_dists.get = function(x, y) {
    return this[x + "," + y];
  };
  var cands = (Object.keys(dists).filter(function(p){
    return dists[p] == Math.min(worst_dist, level);
  }));
  var pick = cands[Math.floor(Math.random() * cands.length)].split(',');
  difficulty = worst_dist;
  return {x:parseInt(pick[0]), y:parseInt(pick[1])};
}

init();
